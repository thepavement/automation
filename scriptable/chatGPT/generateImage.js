// Get the prompt from Shortcuts
let input = args.shortcutParameter;

// OpenAI API key (store it securely in Scriptable or another secure location)
let apiKey = Keychain.get(input.key);
let prompt = input.prompt;

// API endpoint
let url = "https://api.openai.com/v1/images/generations";

// Request payload
let requestBody = {
  prompt: prompt,
  size: "1024x1024",
  n: 1
};

// Set up the request
let req = new Request(url);
req.method = "POST";
req.headers = {
  "Content-Type": "application/json",
  "Authorization": `Bearer ${apiKey}`
};
req.body = JSON.stringify(requestBody);

// Send the request and get the response
let response = await req.loadJSON();
let imageUrl = response.data[0].url;

// Return the image URL to Shortcuts
Script.setShortcutOutput(imageUrl);
Script.complete();

// Get the key passed from Shortcuts
let key = args.shortcutParameter;

// Remove the key from the Keychain
if (Keychain.contains(key)) {
  Keychain.remove(key);

  // Return a confirmation message to Shortcuts
  Script.setShortcutOutput(`Key "${key}" removed successfully.`);
} else {
  // Return a message to Shortcuts if the key does not exist
  Script.setShortcutOutput(`Key "${key}" not found.`);
}

Script.complete();

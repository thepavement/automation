// Get the key and value passed from Shortcuts
let keyValuePair = args.shortcutParameter;

// Extract the key and value
let key = keyValuePair.key;
let value = keyValuePair.value;

// Store the key-value pair in the Keychain
Keychain.set(key, value);

/*
// Show an alert to confirm the operation
let alert = new Alert();
alert.title = "Keychain Updated";
alert.message = `The key "${key}" has been set with the value: ${value}`;
alert.addAction("OK");
await alert.present();
*/

// Return a confirmation message to Shortcuts
Script.setShortcutOutput(`Key "${key}" set successfully.`);
Script.complete();

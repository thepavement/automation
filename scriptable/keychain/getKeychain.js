// Get the key passed from Shortcuts
let key = args.shortcutParameter;

// Retrieve the value from the Keychain
let value = Keychain.get(key);

/*
// Show an alert with the retrieved value
let alert = new Alert();
alert.title = "Key Value";
alert.message = `The value for the key "${key}" is: ${value}`;
alert.addAction("OK");
await alert.present();
*/

// Return the value to Shortcuts
Script.setShortcutOutput(value);
Script.complete();
